# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }


ruby '2.5.5'

gem 'bootsnap', '>= 1.1.0', require: false
gem 'business_time'
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'paranoia', '~> 2.1'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.3'
gem 'rails_admin', '~> 2.0'
gem 'sass-rails', '~> 5.0'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'
gem 'sequenced', '~> 3.2'
gem 'ancestry'
gem 'pdfkit'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'faker'
  gem 'fuubar'
  gem 'guard-rspec'
  gem 'rspec-rails'
  gem 'simplecov', require: false
  gem 'simplecov-cobertura', require: false
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rename'
  gem 'rubocop', require: false
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'rspec_junit_formatter'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

gem 'activerecord-nulldb-adapter'
gem 'groupdate'
gem 'pghero'
gem 'rollbar'

# set up enviroment to file
gem 'dotenv-rails'

# set up API grape and swagger doc
gem 'grape'
gem 'grape-entity'
gem 'grape-middleware-logger'
gem 'grape_on_rails_routes'
gem 'hashie-forbidden_attributes'

# documentation
gem 'grape-swagger'
gem 'grape-swagger-entity'
gem 'grape-swagger-rails'
gem 'grape-swagger-representable'

# user auth
gem 'devise'
gem 'doorkeeper'
gem 'oauth2'
gem 'wine_bouncer'

gem 'awesome_rails_console'

# worker
gem 'daemons'
gem 'delayed_job_active_record'

# pagination
gem 'api-pagination'
gem 'kaminari'

# api request
gem 'rest-client'

gem 'fcm'
gem 'google-cloud-storage', '~> 1.11', require: false
gem 'image_processing', '~> 1.0'
gem 'mail'
gem 'parallel', '~> 1.19', '>= 1.19.1'
gem 'prometheus-client'
gem 'rack-cors'
gem 'spreadsheet_architect', '~> 3.3', '>= 3.3.1'
gem 'whenever', require: false
