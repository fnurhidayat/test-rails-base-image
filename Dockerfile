FROM ruby:2.5.5-alpine3.10
LABEL maintainer="fnurhidayat@binar.co.id"
ENV HOME="/opt/app-root"
RUN set -xe \
 && apk add --no-cache postgresql-client nodejs tini tzdata libc6-compat \
 && mkdir -p ${HOME}
RUN apk add --update --no-cache \
 libgcc libstdc++ libx11 glib libxrender libxext libintl \
 ttf-dejavu ttf-droid ttf-freefont ttf-liberation ttf-ubuntu-font-family
COPY --from=madnight/alpine-wkhtmltopdf-builder:0.12.5-alpine3.10-606718795 \
 /bin/wkhtmltopdf /bin/wkhtmltopdf
WORKDIR ${HOME}
RUN set -xe \
 && bundle config --global frozen 1 && bundle config --local build.sassc --disable-march-tune-native
COPY Gemfile Gemfile.lock ./
RUN set -xe \
 && apk add --no-cache --virtual build-deps build-base postgresql-dev \
 && bundle install \
 && apk del build-deps
COPY . .
ENTRYPOINT ["/sbin/tini", "--"]
